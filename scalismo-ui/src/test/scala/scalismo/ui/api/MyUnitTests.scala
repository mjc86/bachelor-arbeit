/*
 * Copyright (C) 2016  University of Basel, Graphics and Vision Research Group 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package scalismo.ui.api

import java.io.File

import org.scalatest.{ FunSpec, Matchers }
import scalismo.geometry.{ Point, Point3D, Vector3D, _3D }
import scalismo.io.StatismoIO
import scalismo.mesh.TriangleMesh
import scalismo.registration.{ RigidTransformation, RotationTransform, Transformation, TranslationTransform }

class MyUnitTests extends FunSpec with Matchers {

  describe("a group") {
    it("doesn't transforms if there is no transformation") {
      val ui = ScalismoUI()

      val testPoints = IndexedSeq(Point3D(1, 0, 0), Point3D(0, 1, 0))
      val group = ui.createGroup("abc")
      val pointsView = ui.show(group, testPoints, "points")
      val points: IndexedSeq[Point[_3D]] = pointsView.transformedPoints

      points(0).x should be(1.0)
      points(0).y should be(0.0)
      points(1).x should be(0.0)
      points(1).y should be(1.0)

      ui.close()
    }

    it("generic transformation") {
      val ui = ScalismoUI()

      val testPoints = IndexedSeq(Point3D(1, 2, 3), Point3D(-3, -2, -1))
      val group = ui.createGroup("abc")
      val pointsView = ui.show(group, testPoints, "points")
      val transformation = Transformation((p: Point[_3D]) => Point3D(-2 * p.x, p.y, 2 * p.z))
      ui.addTransformation(group, transformation, "transformation")
      val points: IndexedSeq[Point[_3D]] = pointsView.transformedPoints

      points(0).x should be(-2.0)
      points(0).y should be(2.0)
      points(0).z should be(6.0)
      points(1).x should be(6.0)
      points(1).y should be(-2.0)
      points(1).z should be(-2.0)

      ui.close()
    }

    it("rigid transformation") {
      val ui = ScalismoUI()

      val testPoints = IndexedSeq(Point3D(1, 0, 0))
      val group = ui.createGroup("abc")
      val pointsView = ui.show(group, testPoints, "points")
      val translation = TranslationTransform(Vector3D(1.0, 1.0, 1.0))
      val rotation = RotationTransform(math.Pi, 0, 0, Point3D(0, 0, 0))
      // Point(1,0,0) -> rotate (pi,0,0) -> Point(-1,0,0)
      // -> translate(1,1,1) -> Point(0,1,1)
      val transformation = RigidTransformation(translation, rotation)
      ui.addTransformation(group, transformation, "transformation")
      val points: IndexedSeq[Point[_3D]] = pointsView.transformedPoints

      points(0).x should be(0.0 +- 0.00001)
      points(0).y should be(1.0 +- 0.00001)
      points(0).z should be(1.0 +- 0.00001)

      ui.close()
    }
  }

  describe("a subgroup structure") {
    it("transforms is there is a transformation in a supergroup") {

      val ui = ScalismoUI()

      val testPoints = IndexedSeq(Point3D(1, 0, 0), Point3D(0, 1, 0))
      val group = ui.createGroup("abc")
      val subgroup = ui.createGroup("subgroup", group)
      val pointsView = ui.show(subgroup, testPoints, "points")
      val translation = TranslationTransform(Vector3D(1.0, 1.0, 0.0))
      ui.addTransformation(group, translation, "translation")
      val points: IndexedSeq[Point[_3D]] = pointsView.transformedPoints

      points(0).x should be(2.0)
      points(0).y should be(1.0)
      points(1).x should be(1.0)
      points(1).y should be(2.0)

      ui.close()
    }

    it("combined transformations") {
      val ui = ScalismoUI()

      val testPoints = IndexedSeq(Point3D(1, 0, 1), Point3D(0, 1, 0))
      val group = ui.createGroup("abc")
      val subgroup = ui.createGroup("subgroup", group)
      val pointsView = ui.show(subgroup, testPoints, "points")
      val transformation = Transformation((p: Point[_3D]) => Point3D(2 * p.x, 2 * p.y, 2 * p.z))
      val inversion = Transformation((p: Point[_3D]) => Point3D(-p.x, -p.y, -p.z))
      ui.addTransformation(group, transformation, "transformation")
      ui.addTransformation(subgroup, inversion, "inversion")
      val points: IndexedSeq[Point[_3D]] = pointsView.transformedPoints

      points(0).x should be(-2.0)
      points(0).y should be(0.0)
      points(0).z should be(-2.0)
      points(1).x should be(0.0)
      points(1).y should be(-2.0)
      points(1).z should be(0.0)

      ui.close()
    }

    it("more than one transformation on each level") {
      val ui = ScalismoUI()

      val testPoints = IndexedSeq(Point3D(1, 0, 1), Point3D(0, 1, 0))
      val group = ui.createGroup("abc")
      val subgroup = ui.createGroup("subgroup", group)
      val pointsView = ui.show(subgroup, testPoints, "points")
      val flipX = Transformation((p: Point[_3D]) => Point3D(-p.x, p.y, p.z))
      val double = Transformation((p: Point[_3D]) => Point3D(2 * p.x, 2 * p.y, 2 * p.z))
      val translation = TranslationTransform(Vector3D(2.0, 3.0, 1.0))
      val inversion = Transformation((p: Point[_3D]) => Point3D(-p.x, -p.y, -p.z))
      ui.addTransformation(group, flipX, "flip X")
      ui.addTransformation(group, double, "double")
      ui.addTransformation(subgroup, translation, "translation")
      ui.addTransformation(subgroup, inversion, "inversion")
      val points: IndexedSeq[Point[_3D]] = pointsView.transformedPoints

      points(0).x should be(6.0)
      points(0).y should be(-6.0)
      points(0).z should be(-4.0)
      points(1).x should be(4.0)
      points(1).y should be(-8.0)
      points(1).z should be(-2.0)

      ui.close()
    }

    it("applies transformations to nested subgroups") {
      val ui = ScalismoUI()

      val testPoints = IndexedSeq(Point3D(1, 0, 0), Point3D(0, 1, 0))
      val group = ui.createGroup("abc")
      val subgroup = ui.createGroup("subgroup", group)
      val subsubgroup = ui.createGroup("subsubgroup", subgroup)
      val pointsView1 = ui.show(subgroup, testPoints, "points")
      val pointsView2 = ui.show(subsubgroup, testPoints, "points")
      val translation = TranslationTransform(Vector3D(1.0, 1.0, 0.0))
      ui.addTransformation(group, translation, "translation")
      val points1: IndexedSeq[Point[_3D]] = pointsView1.transformedPoints
      val points2: IndexedSeq[Point[_3D]] = pointsView2.transformedPoints

      points1(0).x should be(2.0)
      points1(0).y should be(1.0)
      points1(1).x should be(1.0)
      points1(1).y should be(2.0)

      points2(0).x should be(2.0)
      points2(0).y should be(1.0)
      points2(1).x should be(1.0)
      points2(1).y should be(2.0)

      ui.close()
    }

    it("applies transformations in cascade to nested subgroups") {
      val ui = ScalismoUI()

      val testPoints = IndexedSeq(Point3D(1, 0, 0), Point3D(0, 1, 0))
      val group = ui.createGroup("abc")
      val subgroup = ui.createGroup("subgroup", group)
      val subsubgroup = ui.createGroup("subsubgroup", subgroup)
      val pointsView = ui.show(subsubgroup, testPoints, "points")

      val translation = TranslationTransform(Vector3D(1.0, 1.0, 0.0))
      ui.addTransformation(group, translation, "translation")
      ui.addTransformation(subgroup, translation, "translation")

      val points1: IndexedSeq[Point[_3D]] = pointsView.transformedPoints

      points1(0).x should be(3.0)
      points1(0).y should be(2.0)
      points1(1).x should be(2.0)
      points1(1).y should be(3.0)

      ui.close()
    }
  }

  describe("siblings groups") {
    it("a transformation doesn't affect a sibling group") {
      val ui = ScalismoUI()

      val testPoints = IndexedSeq(Point3D(1, 0, 0), Point3D(0, 1, 0))
      val group1 = ui.createGroup("a")
      val group2 = ui.createGroup("b")
      val pointsView = ui.show(group1, testPoints, "points")
      val translation = TranslationTransform(Vector3D(1.0, 1.0, 0.0))
      ui.addTransformation(group2, translation, "translation")
      val points: IndexedSeq[Point[_3D]] = pointsView.transformedPoints

      points(0).x should be(1.0)
      points(0).y should be(0.0)
      points(1).x should be(0.0)
      points(1).y should be(1.0)

      ui.close()
    }

    it("applies a transformation to every subgroup") {
      val ui = ScalismoUI()

      val testPoints = IndexedSeq(Point3D(1, 0, 0), Point3D(0, 1, 0))
      val group = ui.createGroup("abc")
      val subgroup1 = ui.createGroup("subgroup", group)
      val subgroup2 = ui.createGroup("subgroup", group)
      val pointsView1 = ui.show(subgroup1, testPoints, "points")
      val pointsView2 = ui.show(subgroup2, testPoints, "points")
      val translation = TranslationTransform(Vector3D(1.0, 1.0, 0.0))
      ui.addTransformation(group, translation, "translation")
      val points1: IndexedSeq[Point[_3D]] = pointsView1.transformedPoints
      val points2: IndexedSeq[Point[_3D]] = pointsView2.transformedPoints

      points1(0).x should be(2.0)
      points1(0).y should be(1.0)
      points1(1).x should be(1.0)
      points1(1).y should be(2.0)

      points2(0).x should be(2.0)
      points2(0).y should be(1.0)
      points2(1).x should be(1.0)
      points2(1).y should be(2.0)

      ui.close()
    }
  }

  describe("shape transformation") {
    /*it("gt precision without subgroups") {
      val ui = ScalismoUI()
      val ssm = StatismoIO.readStatismoMeshModel(new File("data/cranium-ssm.h5")).get
      val gt = ssm.referenceMesh

      val group = ui.createGroup("group")
      val ssmView = ui.show(group, ssm, "ssm")

      val gtPoints = gt.pointSet.points
      val ssmPoints = ssmView.meshView.transformedTriangleMesh.pointSet.points

      for ((gtPoint, ssmPoint) <- gtPoints.zip(ssmPoints)) {
        (gtPoint - ssmPoint).norm should be < 0.01
      }
      ui.close()
    }

    it("gt remain stable in generic transformations") {
      val ui = ScalismoUI()
      val ssm = StatismoIO.readStatismoMeshModel(new File("data/cranium-ssm.h5")).get
      val translation = (pt: Point[_3D]) => Point(pt.x * 2.0, pt.y * 1.5, pt.z)
      val rotation = RotationTransform(0, math.Pi / 4, 0, Point3D(0, 0, 0))

      // Transformed meshes without ScalismoUI
      val gt1 = ssm.referenceMesh.transform(translation)
      val gt2 = ssm.referenceMesh.transform(rotation).transform(translation)
      val group = ui.createGroup("group")
      val subgroup: Group = ui.createGroup("subgroup", group)

      ui.addTransformation(group, translation, "free form transformation")

      val ssmView = ui.show(subgroup, ssm, "cranium")

      // Before rotation
      val transformedMesh = ssmView.meshView.transformedTriangleMesh
      val translatedPoints1 = transformedMesh.pointSet.points

      ui.addTransformation(subgroup, rotation, "rotation")

      // After rotation
      val transformedMesh2 = ui.find[TriangleMeshView](group, (_: TriangleMeshView) => true).get.transformedTriangleMesh
      val translatedPoints2 = transformedMesh2.pointSet.points

      // compare to gt
      //norm between (before rotation and gt) and (after rotation and gt) should be similar
      val gtPoints1 = gt1.pointSet.points
      val gtPoints2 = gt2.pointSet.points

      val norm1 = (gtPoints1 zip translatedPoints1).map({ case (gtPoint, ssmPoint) => (gtPoint - ssmPoint).norm })
      val norm2 = (gtPoints2 zip translatedPoints2).map({ case (gtPoint, ssmPoint) => (gtPoint - ssmPoint).norm })

      for ((x, y) <- norm1.zip(norm2)) {
        x should be(y +- 0.01)
      }
      ui.close()
    }

    it("gt remains stable in gp") {
      val ui = ScalismoUI()
      val ssm = StatismoIO.readStatismoMeshModel(new File("data/cranium-ssm.h5")).get
      val translation = (pt: Point[_3D]) => Point(pt.x * 2.0, pt.y * 1.5, pt.z)
      val rotation = RotationTransform(0, math.Pi/4, 0, Point3D(0, 0, 0))

      // Transformed meshes without ScalismOUI
      val gt1 = ssm.referenceMesh.transform(translation)
      val gt2 = ssm.referenceMesh.transform(rotation).transform(translation)
      val group = ui.createGroup("group")
      val subgroup: Group = ui.createGroup("subgroup", group)

      ui.addTransformation(group, translation, "free form transformation")

      val ssmView = ui.show(subgroup, ssm, "cranium")

      // Apply shape transformation
      ssmView.shapeModelTransformationView.shapeTransformationView.coefficients =
        ssmView.shapeModelTransformationView.shapeTransformationView.coefficients.map(_ => 1.0)

      // Before rotation
      val transformedMesh = ssmView.meshView.transformedTriangleMesh
      val translatedPoints1 = transformedMesh.pointSet.points

      ui.addTransformation(subgroup, rotation, "rotation")

      // After rotation
      val transformedMesh2 = ui.find[TriangleMeshView](group, (_: TriangleMeshView) => true).get.transformedTriangleMesh
      val translatedPoints2 = transformedMesh2.pointSet.points

      // compare to gt
      // norm between (before rotation and gt1) and (after rotation and gt2) should be similar
      val gtPoints1 = gt1.pointSet.points
      val gtPoints2 = gt2.pointSet.points

      val norm1 = (gtPoints1 zip translatedPoints1).map({ case (gtPoint, ssmPoint) => (gtPoint - ssmPoint).norm })
      val norm2 = (gtPoints2 zip translatedPoints2).map({ case (gtPoint, ssmPoint) => (gtPoint - ssmPoint).norm })

      for ((x, y) <- norm1.zip(norm2)) {
        x should be(y +- 0.1)
      }
      ui.close()
    }*/

    it("gp affects the same way to a subgroup structure") {
      val ui = ScalismoUI()
      val ssm = StatismoIO.readStatismoMeshModel(new File("data/cranium-ssm.h5")).get
      val translation = (pt: Point[_3D]) => Point(pt.x * 2.0, pt.y * 1.5, pt.z)
      val rotation = RotationTransform(0, math.Pi / 4, 0, Point3D(0, 0, 0))
      val group = ui.createGroup("group")
      val group1 = ui.createGroup("group1")
      val subgroup1: Group = ui.createGroup("subgroup", group1)

      // gp after transformation in same group
      ui.addTransformation(group, translation, "free form transformation")
      ui.addTransformation(group, rotation, "rotation")
      val ssmView = ui.show(group, ssm, "cranium 1")
      val coeffs = ssmView.shapeModelTransformationView.shapeTransformationView.coefficients.map(_ => 1.0)
      ssmView.shapeModelTransformationView.shapeTransformationView.coefficients = coeffs
      val points1 = ssmView.meshView.transformedTriangleMesh.pointSet.points

      // gp after transformations in group and subgroup
      ui.addTransformation(group1, rotation, "rotation")
      ui.addTransformation(subgroup1, translation, "translation")
      val ssmView2 = ui.show(subgroup1, ssm, "cranium 2")
      ssmView2.shapeModelTransformationView.shapeTransformationView.coefficients = coeffs
      val points2 = ssmView2.meshView.transformedTriangleMesh.pointSet.points

      // gp affects the same way to both approaches
      for ((point1, point2) <- points1.zip(points2)) {
        (point1 - point2).norm should be(0.0)
      }
      ui.close()
    }
  }
}