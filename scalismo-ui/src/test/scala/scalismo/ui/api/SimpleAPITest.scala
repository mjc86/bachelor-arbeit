/*
 * Copyright (C) 2016  University of Basel, Graphics and Vision Research Group 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package scalismo.ui.api

import java.io.File

import scalismo.geometry.{ Point3D, Vector3D, _3D }
import scalismo.io.MeshIO
import scalismo.mesh.TriangleMesh
import scalismo.registration.{ RigidTransformation, RotationTransform, TranslationTransform }

object SimpleAPITest {

  def main(args: Array[String]): Unit = {

    scalismo.initialize()
    val ui = ScalismoUI()

    // load renderables
    val mesh: TriangleMesh[_3D] = MeshIO.readMesh(new File("data/cranium.stl")).get
    //val image: DiscreteScalarImage[_3D, Float] = ???
    val points = IndexedSeq(Point3D(1, 0, 0), Point3D(0, 1, 0))

    // add callback to the scene
    ui.onGroupAdded(f => println("Added" + f))

    val group = ui.createGroup("group 1")

    // show renderables in view
    val meshView: TriangleMeshView = ui.show(group, mesh, "cranium")
    val pointView: PointCloudView = ui.show(group, points, "points")

    // find a view from the scene or a specific group using a predicate
    //val anewmeshview: Option[TriangleMeshView] = ui.find[TriangleMeshView]((m: TriangleMeshView) => true)
    val anewmeshview: Option[TriangleMeshView] = ui.find[TriangleMeshView](group, (m: TriangleMeshView) => true)
    assert(anewmeshview.get == meshView)

    // add a transformation in the scene
    val translation = TranslationTransform(Vector3D(1.0, 1.0, 0.0))
    val rotation = RotationTransform(0, 0, 0, Point3D(0, 0, 0))
    val transformation = RigidTransformation(translation, rotation)
    val tv = ui.addTransformation(group, transformation, "a transformation")

    // the transformation is applied to existing nodes in view
    val transformedPoints = pointView.transformedPoints
    assert(transformedPoints(0).x == 2.0)
    assert(transformedPoints(0).y == 1.0)

    // remove a view from the scene
    pointView.remove()

    // create subgroups
    val subgroup1 = ui.createGroup("subgroup1", group)
    val subgroup2 = ui.createGroup("subgroup2", group)
    val subsubgroup = ui.createGroup("subgroup1-1", subgroup1)

    val tv2 = ui.addTransformation(subgroup1, transformation, "a transformation")

    // filter view from the scene or a specific group
    val tViewList = ui.filter((_: TransformationView) => true)
    assert(tViewList.size == 2)

    // the transformations are applied in cascade to nested subgroups
    val pointView1 = ui.show(subsubgroup, points, "points")
    val transformedPoints1 = pointView1.transformedPoints
    assert(transformedPoints1(0).x == 3.0)
    assert(transformedPoints1(0).y == 2.0)

    //Thread.sleep(1000000L)

    // remove a group
    group.remove()

    // there is no views left in the scene if the root group is removed
    val sceneViewList = ui.filter((_: ObjectView) => true)
    assert(sceneViewList.isEmpty)

    ui.close()
  }
}