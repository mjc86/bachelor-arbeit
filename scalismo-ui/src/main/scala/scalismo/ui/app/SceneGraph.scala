/*
 * Copyright (C) 2016  University of Basel, Graphics and Vision Research Group 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @author: Carlos Mojentale */

/*package scalismo.ui.app

import java.awt.event._
import java.net.URL

import javax.swing._
import javax.swing.tree.{ DefaultMutableTreeNode, DefaultTreeCellRenderer, DefaultTreeModel, TreePath }
import scalismo.ui.api.{ Group, ObjectView, ScalismoUI }

import scala.collection.mutable

class SceneGraph(val ui: ScalismoUI) extends JFrame {*/

/* val root = SceneNode("Scene")
  val rootTree = new DefaultMutableTreeNode(root.name)

  // this map links a JTree node with the actual SceneNode
  var treeMap: mutable.Map[DefaultMutableTreeNode, SceneNode] = mutable.Map(rootTree -> root)

  // this map links a SceneNode with its current view in ScalismoUI
  var viewMap: mutable.Map[SceneNode, ObjectView] = mutable.Map()

  val tree = new JTree(new DefaultTreeModel(rootTree))
  val model: DefaultTreeModel = tree.getModel.asInstanceOf[DefaultTreeModel]
  tree.addMouseListener(mouseAction)

  val transformationGroup: Group = ui.createGroup("Transformations")
  val meshesGroup: Group = ui.createGroup("Meshes")

  def reloadTree(treeNode: DefaultMutableTreeNode): Unit = {
    model.reload(treeNode)
  }

  def reloadTree(): Unit = {
    reloadTree(rootTree)
  }

  private def removeView(node: SceneNode): Unit = {
    viewMap(node).remove()
  }

  private def recursiveRemoveView(node: SceneNode): Unit = {
    removeView(node)
    node.children.foreach(recursiveRemoveView)
    node.remove()
  }

  def removeNode(treeNode: DefaultMutableTreeNode): Unit = {
    treeNode.removeFromParent()
    treeNode.removeAllChildren()
    val node = treeMap(treeNode)
    reloadTree()
    recursiveRemoveView(node)
  }

  private def reloadView(node: SceneNode): Unit = {
    removeView(node)
    val newView = showNode(node)
    viewMap += (node -> newView)
  }

  def recursiveReloadView(node: SceneNode): Unit = {
    reloadView(node)
    node.children.foreach(recursiveReloadView)
  }

  val popupMenu = SceneGraphMenu(this)

  private def showNode(node: SceneNode): ObjectView = node match {
    case MeshNode(name, _, _) => ui.show(meshesGroup, node.asInstanceOf[MeshNode].mesh, name)
    case TransformationNode(name, t, _) => ui.addTransformation(transformationGroup, t, name)
  }

  def addNode(treeNode: DefaultMutableTreeNode, newNode: SceneNode): Unit = {
    val newTreeNode = new DefaultMutableTreeNode(newNode.name)
    treeNode.add(newTreeNode)
    reloadTree(treeNode)
    val node = treeMap(treeNode)
    node(newNode)
    node.traverse()
    val view = showNode(newNode)
    treeMap += (newTreeNode -> newNode)
    viewMap += (newNode -> view)
  }

  def mouseAction: MouseListener = new MouseAdapter {
    override def mousePressed(e: MouseEvent): Unit = {
      if (SwingUtilities.isRightMouseButton(e)) {
        val path = tree.getPathForLocation(e.getX, e.getY)
        if (path != null) {
          tree.setSelectionPath(path)
          path.getLastPathComponent match {
            case selectedNode: DefaultMutableTreeNode => showPopup(path, selectedNode)
            case _ =>
          }
        }
      }
    }
  }

  def showPopup(path: TreePath, selectedNode: DefaultMutableTreeNode): Unit = {
    val pathBounds = tree.getUI.getPathBounds(tree, path)
    val node = treeMap(selectedNode)
    popupMenu.getPopupMenu(node).show(tree, pathBounds.x, pathBounds.y + pathBounds.height)
  }
}
*/ 