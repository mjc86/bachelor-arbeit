/*
 * Copyright (C) 2016  University of Basel, Graphics and Vision Research Group 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @author: Carlos Mojentale */

/*
package scalismo.ui.app

import scalismo.geometry._3D
import scalismo.mesh.{ TriangleMesh, TriangleMesh3D }
import scalismo.registration.Transformation

sealed trait SceneNode {
  val parent: SceneNode
  val name: String
  var children: List[SceneNode] = List[SceneNode]()
  var transformations: List[Transformation[_3D]] = List[Transformation[_3D]]()

  def apply(node: SceneNode): Unit = {
    children = children :+ node
  }

  def remove(node: SceneNode): Unit = {
    children = children.filterNot(n => n == node)
  }

  def remove(): Unit = {
    children = List[SceneNode]()
    removeFromParent()
  }

  def removeFromParent(): Unit = {
    parent.remove(this)
  }

  def traverse(): Unit = {
    children.foreach(x => {
      x.transformations = x.transformations ++ transformations
      x.traverse()
    })
  }
}

case class RootNode(name: String) extends SceneNode {
  override val parent: SceneNode = null
}

case class TransformationNode(name: String, t: Transformation[_3D], override val parent: SceneNode) extends SceneNode {
  transformations = transformations :+ t
}

case class MeshNode(name: String, model: TriangleMesh3D, override val parent: SceneNode) extends SceneNode {
  var mesh: TriangleMesh3D = model

  override def traverse(): Unit = {
    applyTransformations()
  }

  def applyTransformations(): Unit = {
    transformations.foreach(x => {
      mesh = mesh.transform(x)
    })
  }
}

object SceneNode {
  def apply(name: String): RootNode = RootNode(name)

  def apply(name: String, transformation: Transformation[_3D], parent: SceneNode): TransformationNode = TransformationNode(name, transformation, parent)

  def apply(name: String, mesh: TriangleMesh3D, parent: SceneNode): MeshNode = MeshNode(name, mesh, parent)
}

*/
