/*
 * Copyright (C) 2016  University of Basel, Graphics and Vision Research Group 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @author: Carlos Mojentale */

/*
package scalismo.ui.app

import java.awt.event.{ ActionEvent, ActionListener }

import javax.swing.filechooser.FileNameExtensionFilter
import javax.swing.tree.DefaultMutableTreeNode
import javax.swing.{ JFileChooser, JMenuItem, JPopupMenu }
import scalismo.geometry.{ Point3D, Vector3D }
import scalismo.io.MeshIO
import scalismo.registration.{ RotationTransform, TranslationTransform }

class SceneGraphMenu(scene: SceneGraph) {

  private def addTranslationActionListener: ActionListener = {
    _: ActionEvent =>
      {
        val selectedNode = scene.tree.getLastSelectedPathComponent.asInstanceOf[DefaultMutableTreeNode]
        val translation = TranslationTransform(Vector3D(0, 0, 0))
        val translationNode = SceneNode("Translation", translation, scene.treeMap(selectedNode))
        scene.addNode(selectedNode, translationNode)
      }
  }

  private def addRotationActionListenr: ActionListener = {
    _: ActionEvent =>
      {
        val selectedNode = scene.tree.getLastSelectedPathComponent.asInstanceOf[DefaultMutableTreeNode]
        val rotation = RotationTransform(0f, 0f, 0f, Point3D(0, 0, 0))
        val rotationNode = SceneNode("Rotation", rotation, scene.treeMap(selectedNode))
        scene.addNode(selectedNode, rotationNode)
      }
  }

  def addMeshActionListener: ActionListener = {
    _: ActionEvent =>
      {
        val selectedNode = scene.tree.getLastSelectedPathComponent.asInstanceOf[DefaultMutableTreeNode]
        val chooser = new JFileChooser()
        chooser.setDialogTitle("Select a triangle Mesh")
        chooser.setAcceptAllFileFilterUsed(false)
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Triangle Mesh(*.stl, *vtk, *.h5)", "stl", "h5", "vtk"))
        val returnValue = chooser.showOpenDialog(scene.tree)
        if (returnValue == JFileChooser.APPROVE_OPTION) {
          MeshIO.readMesh(chooser.getSelectedFile) match {
            case scala.util.Failure(_) =>
            case scala.util.Success(mesh) =>
              val node = SceneNode(chooser.getSelectedFile.getName, mesh, scene.treeMap(selectedNode))
              scene.addNode(scene.tree.getLastSelectedPathComponent.asInstanceOf[DefaultMutableTreeNode], node)
          }
        }
      }
  }

  private def removeActionListener: ActionListener = {
    _: ActionEvent =>
      {
        val selectedNode = scene.tree.getLastSelectedPathComponent.asInstanceOf[DefaultMutableTreeNode]
        scene.removeNode(selectedNode)
      }
  }

  private def getMeshMenuItem: JMenuItem = {
    val item = new JMenuItem("Add mesh")
    item.addActionListener(addMeshActionListener)
    item
  }

  private def getTranslationMenuItem: JMenuItem = {
    val item = new JMenuItem("Add translation")
    item.addActionListener(addTranslationActionListener)
    item
  }

  private def getRotationMenuItem: JMenuItem = {
    val item = new JMenuItem("Add rotation")
    item.addActionListener(addRotationActionListenr)
    item
  }

  def getRemoveMenuItem: JMenuItem = {
    val item = new JMenuItem("Remove Node")
    item.addActionListener(removeActionListener)
    item
  }

  private def getMenuItems(node: SceneNode): List[JMenuItem] = {
    node match {
      case RootNode(_) => List(getMeshMenuItem, getTranslationMenuItem, getRotationMenuItem)
      case TransformationNode(_, _, _) => List(getMeshMenuItem, getTranslationMenuItem, getRotationMenuItem, getRemoveMenuItem)
      case MeshNode(_, _, _) => List(getRemoveMenuItem)
    }
  }

  def getPopupMenu(node: SceneNode): JPopupMenu = {
    val menu = new JPopupMenu()
    getMenuItems(node).foreach(i => menu.add(i))
    menu
  }
}

object SceneGraphMenu {
  def apply(scene: SceneGraph): SceneGraphMenu = new SceneGraphMenu(scene)
}
*/
