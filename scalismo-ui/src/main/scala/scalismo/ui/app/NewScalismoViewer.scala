/*
 * Copyright (C) 2016  University of Basel, Graphics and Vision Research Group 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @author: Carlos Mojentale */

/*
package scalismo.ui.app

import java.awt.Dimension

import javax.swing.JFrame
import scalismo.ui.api.ScalismoUI

import scala.util.Random

object NewScalismoViewer {
  def main(args: Array[String]): Unit = {
    val frame = new JFrame()
    scalismo.initialize()
    //implicit val rnd = Random(50L)
    val ui = ScalismoUI()
    val sceneGraph = new SceneGraph(ui)
    frame.getContentPane.add(sceneGraph.tree)
    frame.setPreferredSize(new Dimension(400, 800))
    frame.pack()
    frame.setVisible(true)
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  }
}
*/
