/*
 * Copyright (C) 2016  University of Basel, Graphics and Vision Research Group 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package scalismo.ui.app.model

import java.io.File

import breeze.linalg.{ DenseVector, rank }
import scalismo.common.NearestNeighborInterpolator
import scalismo.geometry.Vector3D
import scalismo.io.{ MeshIO, StatismoIO }
import scalismo.registration.{ GaussianProcessTransformation, TranslationTransform }
import scalismo.ui.api.ScalismoUI

object Main {
  def main(args: Array[String]): Unit = {

    //init
    scalismo.initialize()
    val ui = ScalismoUI("Scalismo Viewer")
    val group = ui.createGroup("group")
    val triangleMesh = MeshIO.readMesh(new File("data/mandible.stl")).get
    val ssm = StatismoIO.readStatismoMeshModel(new File("data/cranium-ssm.h5")).get
    val translationTransform = TranslationTransform(Vector3D(0, 0, 0))

    // Scene Graph
    val root = RootNode("root")
    val translation = TranslationNode("translation", translationTransform)
    val translation2 = TranslationNode("translation", translationTransform)
    val triangleLeaf = TriangleMeshNode("triangleMesh", triangleMesh)
    val ssmLeaf = StatisticalMeshNode("ssmMesh", ssm)

    // Shape Transformation
    val gp = ssm.gp.interpolate(NearestNeighborInterpolator())
    val coeff = DenseVector.fill[Double](gp.rank, 3)
    val shapeNode = ShapeTransformationNode("shape", GaussianProcessTransformation(gp, coeff))

    root.add(translation)
    translation.add(translation2)
    translation2.add(shapeNode)
    translation2.add(triangleLeaf)
    shapeNode.add(ssmLeaf)

    root.traverse()

    ui.show(group, triangleLeaf.transformedMesh, triangleLeaf.name)
    ui.show(group, ssmLeaf.transformedMesh, ssmLeaf.name)
  }
}
