/*
 * Copyright (C) 2016  University of Basel, Graphics and Vision Research Group 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package scalismo.ui.app.model

import scalismo.geometry._3D
import scalismo.registration.{ Transformation, TranslationTransform }

case class TranslationNode(name: String, translation: TranslationTransform[_3D]) extends SceneNode with Traversable {

  def add(node: Traversable): Unit = {
    children = children :+ node.asInstanceOf[SceneNode]
  }

  def add(node: TriangleMeshNode): Unit = {
    children = children :+ node.asInstanceOf[SceneNode]
  }

  def add(node: StatisticalMeshNode): Unit = {
    children = children :+ node.asInstanceOf[SceneNode]
  }

  override def traverse(transformations: List[Transformation[_3D]]): Unit = {
    val newTransformations = transformations :+ translation
    children.foreach {
      case node: Traversable => node.traverse(newTransformations)
      case node: TriangleMeshNode => node.applyTransformations(newTransformations)
      case node: StatisticalMeshNode => node.applyTransformations(newTransformations)
    }
  }
}
